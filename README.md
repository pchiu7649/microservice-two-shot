# Wardrobify

Team:

* Peter Chiu - Hats microservice
* Josh James - Shoes microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.
In shoe_rest.models Bin Value Object used tto connect wardrobe with the shoes app. Shoe entity was created to hold fields that were used to create view functions that that used method the CRUD any of the properties used in the Encoders mainly using ShoeDetailEncoder.

## Hats microservice

The Hat model holds the values of the hat
LocationVO is created to placehold the value of locations from the polling service.
The hat Polling service takes location data from the wardrobe and creates LocationVOs from this data.
LocationVO is then bundled with the Hat data and passed to the hats microservice.
