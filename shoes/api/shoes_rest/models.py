from django.db import models
from django.urls import reverse

# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    
class Shoe(models.Model):
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=50)

    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE
    )

    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"pk": self.pk})
