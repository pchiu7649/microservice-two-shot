import json
import requests
import os

from shoes_rest.keys import PEXELS_API_KEY


def get_photo(name, color):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f" {name} {color}",
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": "https://i.pinimg.com/564x/8e/8c/f0/8e8cf0ff242dc8835619a8a8800fa6c0.jpg"}
