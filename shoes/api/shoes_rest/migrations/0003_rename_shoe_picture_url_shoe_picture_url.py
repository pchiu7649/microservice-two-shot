# Generated by Django 4.0.3 on 2023-07-20 14:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0002_rename_name_binvo_closet_name'),
    ]

    operations = [
        migrations.RenameField(
            model_name='shoe',
            old_name='shoe_picture_url',
            new_name='picture_url',
        ),
    ]
