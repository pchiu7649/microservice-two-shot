from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from hats_rest.models import Hat, LocationVO
from common.json import ModelEncoder
from .acls import get_photo

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name","import_href"]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

@require_http_methods(['GET','POST'])
def api_list_hats(request, location_vo_id=None):

    if request.method == "GET":

        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()

        return JsonResponse(
            {"hats": hats},
            encoder=HatDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        photo = get_photo(content["color"], content["style_name"])
        content.update(photo)
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET","DELETE"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()

        return JsonResponse(
            {"deleted": count > 0},
        )