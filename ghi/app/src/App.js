import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import NewHatForm from './NewHatForm';
import ShoeList from './ShoeList';
import NewShoeForm from './NewShoeForm';

function App({hats, shoes}) {
  return (
    <BrowserRouter>
      <Nav/>
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} >
            <Route path="hats" element={<HatsList hats={hats} />} />
            <Route path="new-hat" element={<NewHatForm />} />
            <Route path="shoes" element={<ShoeList shoes={shoes} />} />
            <Route path="new-shoe" element={<NewShoeForm />} />
          </Route>
        </Routes>

      </div>
    </BrowserRouter>
  );
}

export default App;
