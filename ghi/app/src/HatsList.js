import { useState, useEffect } from 'react'
import { Outlet } from 'react-router-dom';

function HatsList({hats}){

    const deleteHat = async (event) => {
        const value = event.target.value;

        const locationUrl = `http://localhost:8090${value}`;

        const fetchConfig = {
            method:"delete",
            header: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {

            window.location.reload();
        }
    }

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Hat</th>
                        <th>Location</th>
                    </tr>
                </thead>
                <tbody>
                    {hats ? hats.map(hat => {
                        return(
                            <tr key={hat.href}>
                                <td>{hat.color} {hat.fabric} {hat.style_name}</td>
                                <td>{hat.location.closet_name}</td>
                                <td>
                                    <button className="button" value={hat.href} onClick={deleteHat}>Delete</button>
                                </td>
                            </tr>
                        );
                    }): <tr><td>There's nothing here</td></tr>}
                </tbody>
            </table>


        </>
    );
}

export default HatsList
