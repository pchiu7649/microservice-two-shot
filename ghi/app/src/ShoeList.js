function ShoeList({shoes}) {
    const deleteShoe = async (event) => {
        const value = event.target.value;
        console.log(value)

        const binUrl = `http://localhost:8080${value}`;

        const fetchConfig = {
            method:"delete",
            header: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(binUrl, fetchConfig);
        if (response.ok) {

            window.location.reload();
        }
    }


  return (
      <>
      <table className="table table-striped">
      <thead>
        <tr>
          <th>Shoe Pic</th>
          <th>Name</th>
          <th>Color</th>
          <th>Closet Name</th>
        </tr>
      </thead>
      <tbody>
        {shoes ? shoes.map(shoe => {
          console.log(shoe)
          return(
            <tr key={shoe.href}>
              <td>
                <img src={shoe.picture_url} className="card-img-top"/>
              </td>
              <td>{ shoe.name }</td>
              <td>{ shoe.color }</td>
              <td>{ shoe.bin.closet_name }</td>
              <td>
                <button className="button" value={shoe.href} onClick={deleteShoe}>Delete</button>
              </td>
            </tr>
          );
        }): <tr><td>"Aww, nothng's there"</td></tr>}
      </tbody>
    </table>
    </>
  );
}

export default ShoeList
