import React, {useEffect, useState} from 'react';

function NewShoeForm(){
    
    const [bins, setBins] = useState([]);

    const [name, setName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins([...data.bins]);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bin = bin;

        const binUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'application/json',
            },
        };
        const shoeResponse = await fetch(binUrl, fetchConfig);
        if (shoeResponse.ok) {
            setName('');
            setColor('');
            setBin('');
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new shoe</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                            <option defaultValue="bin">Choose a bin</option>
                            {bins.map(bin => {
                                return (
                                    <option key={bin.href} value={bin.href}>
                                        {bin.closet_name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        </div>
    )
}

export default NewShoeForm
